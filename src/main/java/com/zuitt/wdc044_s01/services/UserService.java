package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.User;

import java.util.Optional;

public interface UserService {
    void createUser(User user);
    void updateUser(Long id, User user);
    void deleteUser(Long id, User user);
    Iterable<User> getUsers();
    Optional<User> findByUsername(String username);
}
