package com.zuitt.wdc044_s01.exceptions;

public class UserException extends  Exception{
    public UserException(String message) {
        super(message);
    }
}
