package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository<Post, Long> {
}


//In Express, data goes through the process of rout->controller->model->database
//In Spring Boot, data goes through the process of controller->service->repository->model->database